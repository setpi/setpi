#!/bin/bash

SETPI="./dist/build/setpi/setpi"
DIR="examples"
WHITELIST="can-fix.sp key-reg.sp yubikey_single.sp macan.sp"

echo '\begin{tabular}{lll}'
echo '\hline'
echo 'Example & Average time & Attack\\'
echo '\hline'

for f in ${WHITELIST}; do
ftime="$(
    for i in {1..10}; do
        time ${SETPI} ${DIR}/${f};
    done 2>&1 | grep ^real | sed -e s/.*m// | awk '{sum += $1} END {print sum / NR}'
)"
${SETPI} ${DIR}/${f} | grep "RESULT goal reachable" > /dev/null
if [[ $? -eq '1' ]]; then fattack='no'; else fattack='yes'; fi
fname="$(echo ${f} | sed -E 's/([#$%&_\])/\\&/g')"
echo "${fname} & ${ftime}s & ${fattack} \\\\"
done

echo '\hline'
echo '\end{tabular}'
