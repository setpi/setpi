import os, sys, re
import subprocess as sp

PATH = os.path.dirname(__file__) or '.'
SETPI_BIN = os.path.join(PATH, "../dist/build/setpi/setpi")

# result_pattern searches for expected outputs contained as comments in the file:
# (**result** expected_output **)
result_pattern = re.compile(r"[(][*][*]result[*][*]\s+(.+?)\s+[*][*][)]", re.MULTILINE | re.DOTALL)

def test(f):
    print "Testing %0s" % f
    model = open(f).read()
    output = sp.check_output([SETPI_BIN, f])
    for match in re.finditer(result_pattern, model):
        pattern = match.group(1)
        if output.find(pattern) > 0:
            print "Test for `%0s' succeded" % pattern
        else:
            print "Test for `%0s' failed" % pattern

if __name__ == '__main__':
    args = sys.argv
    if len(args) == 2:
        files = [args[1]]
    else:
        files = filter(lambda x: x.endswith('.pv'),
                       os.listdir(PATH))
    print SETPI_BIN
    for f in files:
        test(f)
