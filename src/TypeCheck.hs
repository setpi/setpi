module TypeCheck (typeCheck) where

import Types
import Unification
import Text.Parsec.Pos
import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe

data TypeError = TypeError { teDescr :: String, tePos :: SourcePos }

instance Show TypeError where
    show (TypeError descr pos) = sourceName pos ++ ":" ++ show (sourceLine pos) ++ ":" ++ show (sourceColumn pos) ++ ": "  ++ descr

typeCheck (System process pubnames reducs sets queries funcs types params) =
    typeCheckS sets ++
    typeCheck' process' Map.empty [] sets reducs funcs
    where process' = restrictProcess pubnames process

typeCheckS (SetDecl s (Func TName name []) event pos:ss) = setMulDecl s pos ss ++ typeCheckS ss
typeCheckS (SetDecl s (Func TFunc f [Func TName name []]) event pos:ss) = setMulDecl s pos ss ++ typeCheckS ss
typeCheckS (SetDecl s type_ event pos:ss) =
    setMulDecl s pos ss ++
    [TypeError ("Set declaration for `" ++ s ++ "' has invalid type `" ++ show type_ ++
                "'. Only names and monadic constructors on names are allowed") pos]
typeCheckS [] = []

setMulDecl s pos ss =
    [TypeError ("Set `" ++ s ++ "' is already declared at " ++ show pos') pos |
     SetDecl s' _ _ pos' <- ss, s == s']

unref vars pos gamma =
    [TypeError ("Variable `" ++ show x ++ "' referenced but never defined") pos
     | x <- vars, not(x `Map.member` gamma)]

typeCheck' :: Process -> Gamma -> [Set] -> [SetDecl] -> [Reduc] -> [FuncDecl] -> [TypeError]
typeCheck' Nil _ _ _ _ _ = []
typeCheck' (Par p1 p2 pos) gamma locks sets reducs funcs =
    if locks == [] then
        typeCheck' p1 gamma locks sets reducs funcs ++ 
        typeCheck' p2 gamma locks sets reducs funcs
    else [TypeError ("Locks " ++ show locks ++ " held before parallel composition") pos]
typeCheck' (Repl p pos) gamma locks sets reducs funcs =
    if locks == [] then
        typeCheck' p gamma locks sets reducs funcs
    else [TypeError ("Locks " ++ show locks ++ " held before replication") pos]
typeCheck' (Out ch msg p pos) gamma locks sets reducs funcs =
    unref (fvp ch ++ fvp msg) pos gamma ++
    typeCheck' p gamma locks sets reducs funcs
typeCheck' (In ch x t p pos) gamma locks sets reducs funcs =
    unref (fvp ch) pos gamma ++
    errors ++ typeCheck' p gamma' locks sets reducs funcs
    where (gamma', errors) = matchType x t gamma funcs pos
typeCheck' (New x a p pos) gamma locks sets reducs funcs =
    tvarErr ++ errors ++ typeCheck' p gamma' locks sets reducs funcs
    where (gamma', errors) = matchType x a gamma funcs pos
          tvarErr = case a of
                      Func TName name [] -> []
                      _ -> [TypeError ("Type " ++ show a ++ " is not a basic name type") pos]
typeCheck' (IfE b p1 p2 pos) gamma locks sets reducs funcs =
    unref (fvp b) pos gamma ++
    typeCheckB b gamma locks sets pos ++ 
    typeCheck' p1 gamma locks sets reducs funcs ++ 
    typeCheck' p2 gamma locks sets reducs funcs
typeCheck' (Let t1 ty1 t2 p1 p2 pos) gamma locks sets reducs funcs =
    unref (fvp t2) pos gamma ++
    errors ++
    typeCheck' p1 gamma' locks sets reducs funcs ++
    typeCheck' p2 gamma locks sets reducs funcs
    where (gamma', errors) = matchType t1 ty1 gamma funcs pos
          -- TODO type inference destructor
typeCheck' (SetE b p pos) gamma locks sets reducs funcs =
    unref (fvp b) pos gamma ++
    typeCheckB b gamma locks sets pos ++
    typeCheck' p gamma locks sets reducs funcs
typeCheck' (SelectE x t b p pos) gamma locks sets reducs funcs =
    errors ++ typeCheck' p gamma' locks sets reducs funcs ++
      (case b of Just b -> typeCheckB b gamma' locks sets pos; Nothing -> [])
    where (gamma', errors) = matchType x t gamma funcs pos
typeCheck' (Event s x ty p pos) gamma locks sets reducs funcs =
    typeCheck' (Lock [s] (SetE (SetIn x s) (Unlock [s] p pos) pos) pos) gamma locks sets reducs funcs
typeCheck' (Lock ss p pos) gamma locks sets reducs funcs =
    undefError ++ lockError ++ typeCheck' p gamma (ss ++ locks) sets reducs funcs
    where lockError =
              if not (List.null locked) then
                  [TypeError ("Locks for " ++ show locked ++ " already held in the process") pos]
              else []
          locked = ss `List.intersect` locks
          undefError = [TypeError ("Definition for set `" ++ show s ++ "' not found") pos |
                        s <- ss, let res = List.find (\sd -> setName s == setDeclName sd) sets, res == Nothing]
typeCheck' (Unlock ss p pos) gamma locks sets reducs funcs =
    undefError ++ unlockError ++ typeCheck' p gamma (locks List.\\ ss) sets reducs funcs
    where unlockError =
              if not (List.null unlocked) then
                  [TypeError ("Unlocking " ++ show unlocked ++ " that is already unlocked") pos]
              else []
          unlocked = ss List.\\ locks
          undefError = [TypeError ("Definition for set `" ++ show s ++ "' not found") pos |
                        s <- ss, let res = List.find (\sd -> setName s == setDeclName sd) sets, res == Nothing]

matchType :: Term -> Term -> Gamma -> [FuncDecl] -> SourcePos -> (Gamma, [TypeError])
matchType (Var x) type_ gamma funcs pos =
    case Map.lookup (Var x) gamma of
      Just type_' ->
          if type_ == type_' then (gamma, [])
          else (gamma, [TypeError ("Type mismatch for `" ++ x ++ "': expected " ++ show type_' ++
                                   " but got `" ++ show type_ ++ "'") pos])
      Nothing ->
          (Map.insert (Var x) type_ gamma, [])
matchType (Func TFunc f1 fargs1) (Func TFunc f2 fargs2) gamma funcs pos =
    case List.find (\x->funcDeclName x == f1) funcs of
      Just (FuncDecl f3 fargs3 _ _) ->
          if (f1 == f2) && (List.length fargs1 == List.length fargs2) then
              if (f1 == f3) && (List.length fargs1 == List.length fargs3)
                     && Maybe.isJust (unifyList (Just Map.empty) fargs2 fargs3) then
                  matchType' fargs1 fargs2 gamma funcs pos
              else
                  let (gamma1, errors) = matchType' fargs1 fargs2 gamma funcs pos in
                  (gamma1, TypeError ("Computed type `" ++ show (Func TFunc f2 fargs2) ++
                                      "' for `" ++ show (Func TFunc f1 fargs1) ++
                                      "' does not comform declared type for " ++ f3) pos:errors)
          else (gamma, [TypeError ("Mismatch between term `" ++ show (Func TFunc f1 fargs1) ++ 
                                   "' and type `" ++ show (Func TFunc f2 fargs2) ++ "'") pos])
matchType (Func TTuple f1 fargs1) (Func TTuple f2 fargs2) gamma funcs pos =
    if (f1 == f2) && (List.length fargs1 == List.length fargs2) then
        matchType' fargs1 fargs2 gamma funcs pos
    else (gamma, [TypeError ("Mismatch between term `" ++ show (Func TTuple f1 fargs1) ++ 
                             "' and type `" ++ show (Func TTuple f2 fargs2) ++ "'") pos])
matchType' (t:args1) (ty:args2) gamma funcs pos = (gamma2, errors2)
    where (gamma1, errors1) = matchType t ty gamma funcs pos
          (gamma2, errors2) = matchType' args1 args2 gamma1 funcs pos
matchType' [] [] gamma funcs pos = (gamma, [])

typeCheckB :: SetExp -> Gamma -> [Set] -> [SetDecl] -> SourcePos -> [TypeError]
typeCheckB (SetIn t (Set s)) gamma locks sets pos =
    case List.find (\x -> setDeclName x == s) sets of
      Just (SetDecl _ ty' _ _) ->
          (if ty == ty' then []
           else [TypeError ("Type of `" ++ show t ++ "' is: " ++ show ty ++ 
                        " while `set " ++ s ++ "' has type: " ++ show ty') pos]) ++
          (if not $ (Set s) `List.elem` locks then
               [TypeError ("Operating on unlocked set `" ++ s ++ "'") pos]
           else [])
      Nothing -> [TypeError ("Set `" ++ s ++ "' is not declared") pos]
    where ty = subst gamma t
typeCheckB (And b1 b2) gamma locks sets pos =
    typeCheckB b1 gamma locks sets pos ++ typeCheckB b2 gamma locks sets pos
typeCheckB (Or b1 b2) gamma locks sets pos =
    typeCheckB b1 gamma locks sets pos ++ typeCheckB b2 gamma locks sets pos
typeCheckB (Not b) gamma locks sets pos =
    typeCheckB b gamma locks sets pos
