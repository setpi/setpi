module SpassOut (outSpass) where

import Types
import Unification
import Text.Parsec.Pos

import qualified Data.List as List

outSpass :: PrologProgram -> String
outSpass (PrologProgram queries _ clauses fundecls preddecls _) = 
    template (buildFDefs $ concatMap buildFuncDefsC clauses) (buildPDefs preddecls)
                 (buildClauses clauses) (buildQueries queries)

buildFDefs :: [FuncDecl] -> String
buildFDefs fs = List.intercalate "," (List.map buildFDef fs)
buildFDef (FuncDecl f n p pos) = "(" ++ f ++ "," ++ show n ++ ")"

buildPDefs ps = List.intercalate "," (List.map buildPDef ps)
buildPDef (PredDecl p n) = "(" ++ p ++ "," ++ show n ++ ")"

buildClause (Clause h c) =
    case fvs of
      [] -> "formula(" ++ clause ++ ").\n"
      _  -> "formula(forall([" ++ (List.intercalate "," (List.map show fvs)) ++ "]," ++ clause ++")).\n"
    where fvs = fvList h ++ fv c
          clause = case h of
                     [] -> showPred c
                     _  -> "implies(" ++ andPreds h ++ "," ++ showPred c ++ ")"
          andPreds [p] = showPred p
          andPreds (p:ps) = "and(" ++ showPred p ++ "," ++ andPreds ps ++ ")"
          andPreds [] = error "andPreds: Empty lists not allowed"
buildClauses clauses = List.concatMap buildClause clauses

showPred (Pred p args) = p ++ "(" ++ (List.intercalate "," (List.map showTerm args)) ++ ")"

showTerm (Var x) = x
showTerm (Func _ n args) = n ++ "(" ++ (List.intercalate "," (List.map showTerm args)) ++ ")"

buildFuncDefsC (Clause h c) = List.concat $ buildFuncDefsP c : List.map buildFuncDefsP h
buildFuncDefsP (Pred p args) = concatMap buildFuncDefsT args
buildFuncDefsT (Var x) = []
buildFuncDefsT (Func _ f args) = FuncDecl f args False initPos : concatMap buildFuncDefsT args

initPos = initialPos "spass"

buildQuery q = "formula(" ++ showPred q ++ ").\n"
buildQueries qs = List.concatMap buildQuery qs

template fdefs pdefs rules queries =
    "begin_problem(protocol).\
    \list_of_descriptions.\
    \name({*protocol*}).\
    \author({*setPi compiler*}.\
    \status(unknown).\
    \description({*N/A*}).\
    \end_of_list.\
    \\
    \list_of_symbols.\
    \functions[" ++ fdefs ++ "].\
    \predicates[" ++ pdefs ++ "].\
    \end_of_list.\
    \\
    \list_of_formulae(axioms).\n" ++ 
    rules ++ "\
    \end_of_list.\
    \\
    \list_of_formulae(conjectures).\n" ++
    queries ++ "\
    \end_of_list.\
    \end_problem.\n"
