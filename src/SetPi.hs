module SetPi where

import Types
import Unification

import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import qualified Data.List as List
import Text.Parsec.Pos

-- Activates implies generation optimizations
impliesOptim = False

posId :: SourcePos -> String
posId pos = show (sourceLine pos) ++ "x" ++ show (sourceColumn pos)

data TrClause = OutClause [Pred] Pred VC Gamma
              | ImpliesClause [Pred] [Term] VC VC Gamma
              | TImpliesClause [Term] Pred VC VC Gamma
                deriving (Eq, Show)

-- Applies the type system and the set abstraction from the translated
-- clauses (still abstract), in order to produce Horn clauses.
desugar :: [SetDecl] -> TrClause -> [Clause]
desugar sets (OutClause h c a gamma) = [intVC gamma sets a (Clause [] h c)]
desugar sets (ImpliesClause h ts a a' gamma) =
    [Clause [] (List.map (intVC gamma sets a) h) (Pred "implies" [intVC gamma sets a t, intVC gamma sets a' t]) | t <- ts]
desugar sets (TImpliesClause ts c a1 a2 gamma) =
    [Clause [] [Pred "implies" [intVC gamma sets a1 t, intVC gamma sets a2 t], intVC gamma sets a1 c] (intVC gamma sets a2 c)
     | t <- ts]

translate :: System -> PrologProgram
translate (System process pubNames reducs sets queries funcs types params) =
    PrologProgram { prologQueries   = prQueries,
                    prologNoUnif    = [Pred "att" [Var "x"]],
                    prologClauses   = clPublic ++ clAtt ++ clAttNames ++ clReducs ++ clConstr ++ clProcess ++ clImplies,
                    prologFuncDecls = clVal ++ funcs,
                    prologPredDecls = [PredDecl "att" 1, PredDecl "name" 1, PredDecl "msg" 2, PredDecl "implies" 2],
                    prologParams    = params }
    where
      -- Public names known to the attacker
      clPublic = clPublic' pubNames
          where
            clPublic' Nil = []
            clPublic' (New x (Func TName name []) cont pos) =
                Clause [] [] (Pred "att" [intVC gamma sets a t]) : clPublic' cont
                where t = Func TName ("n" ++ posId pos) []
                      a = zeroVC gamma sets a t
                      gamma = Map.fromList [(t, (Func TName name []))]

      -- Attacker clauses
      clAtt = [Clause [] [Pred "att" [Var "x_ch__"], Pred "att" [Var "x_msg__"]] $ Pred "msg" [Var "x_ch__", Var "x_msg__"],
               Clause [] [Pred "msg" [Var "x_ch__", Var "x_msg__"], Pred "att" [Var "x_ch__"]] $ Pred "att" [Var "x_msg__"],
               Clause [] [Pred "implies" [Var "x", Var "y"], Pred "name" [Var "x"]] $ Pred "name" [Var "y"],
               Clause [] [Pred "implies" [Var "x", Var "y"], Pred "att" [Var "x"]] $ Pred "att" [Var "y"]] ++
              [Clause [] [Pred "implies" [v1, v2], Pred "att" [Func TFunc f [v1]]]
                      (Pred "att" [Func TFunc f [v2]]) |
               FuncDecl f [Func TName tname []] _ _ <- funcs,
               let sets' = sets `ofType` tname, let n = List.length sets', n > 0,
               let v1 = Func TFunc ("val_" ++ tname) (Var "x":[Var ("xvc1_" ++ show i) | i <- [1..n]]),
               let v2 = Func TFunc ("val_" ++ tname) (Var "x":[Var ("xvc2_" ++ show i) | i <- [1..n]])] ++
              [Clause [] [Pred "att" [Var ("x" ++ show i)] | i <- [1..j]]
                      (Pred "att" [mkTuple [Var ("x"++show i) | i <- [1..j]]]) | j <- tupleSizes] ++
              [Clause [] [Pred "att" [mkTuple [Var ("x"++show i) | i <- [1..j]]]] c | j <- tupleSizes,
               c <- [Pred "att" [Var ("x" ++ show i)] | i <- [1..j]]]
      tupleSizes = tupleSizesP process

      -- A name for each type is known to the attacker
      clAttNames = List.concat
                   [[intVC gamma sets a (Clause [] [] (Pred "att" [name])),
                     intVC gamma sets a (Clause [] [] (Pred "name" [name]))] |
                    Func TName tname [] <- types,
                    let name = Func TName ("attn_" ++ tname) [],
                    let gamma = Map.fromList [(name, Func TName tname [])],
                    let a = zeroVC gamma sets [] name]

      -- Generating clauses for destructors definitions
      clReducs = [Clause [] [Pred "att" [arg] | arg <- args] (Pred "att" [res]) |
                  (Reduc _ args res private _) <- reducs, private == False]

      -- Generating clauses for constructor definitions
      clConstr = [Clause [] [Pred "att" [Var $ "x" ++ show i] | i <- [1..n]]
                        $ Pred "att" [Func TFunc f [Var $ "x" ++ show i | i <- [1..n]]] |
                  (FuncDecl f args False _) <- funcs, let n = List.length args]

      -- Translating the process
      trclProcess = trpr (System process pubNames reducs sets queries funcs types params)
                    (restrictProcess pubNames process) Map.empty [] [] [] [] [] []

      -- Producing Horn clauses for the translation
      clProcess = List.concatMap (desugar sets) $ trclProcess
      
      -- Generating the implies clauses
      clImplies = List.concatMap (desugar sets) $ genImplies trclProcess

      -- Value constructor declarations, used for the set-abstraction
      clVal = [FuncDecl ("val_" ++ tname) (Var "x":vcs tname) True (initialPos "header") |
               Func TName tname [] <- types]
          where vcs tname = [Var ("x_vc_" ++ s) | SetDecl s t _ _ <- sets, repr t == Func TName tname []]

      -- Interpreting the queries
      prQueries = [intVC gamma sets a (Pred p pargs) | Query (Pred p pargs) state gamma <- queries,
                   a <- case state of { Just b -> restrict [] b; Nothing -> [[]] }]

genNames :: [Pred] -> [Term] -> VC -> Gamma -> [TrClause]
genNames h disclose a gamma = [OutClause h (Pred "name" [t]) a gamma | t <- disclose]

-- Little optimization: "name" predicate as hypothesis are not
-- generated if no sets are declared for them.
genHyp :: [Term] -> Gamma -> [SetDecl] -> [Pred]
genHyp disclose gamma sets =
    [Pred "name" [t] | t <- disclose,
     let Just (Func TName ty []) = Map.lookup t gamma,
     List.length (sets `ofType` ty) > 0]

-- Tanslates a process into uninterpreted clauses, types and
-- set-abstractions are kept separate from the shape of the predicates
-- in order to ease the generation of implies rules.
trpr :: System -> Process -> Subst -> [Pred] -> [Term] -> [Term] -> [Term] -> [Set] -> VC -> [TrClause]
trpr _ Nil _ _ _ _ _ _ _ = []
trpr sys (Repl p pos) gamma h vi ve priv [] a =
    genNames h priv a gamma ++ trpr sys p gamma h' (Var ("i" ++ posId pos):vi) ve [] [] (relax a [] [])
    where h' = genHyp priv gamma (systemSets sys) ++ h
trpr sys (Par p1 p2 pos) gamma h vi ve priv [] a =
    genNames h priv a gamma ++
    trpr sys p1 gamma h' vi ve [] [] (relax a [] []) ++
    trpr sys p2 gamma h' vi ve [] [] (relax a [] [])
    where h' = genHyp priv gamma (systemSets sys) ++ h
trpr sys (In ch x t p pos) gamma h vi ve priv l a =
    trpr sys (subst rho p) gamma' (Pred "msg" [ch, t'] : h) vi (t' : ve) priv l a'
    where a' = relax a priv l
          (gamma', t') = pt gamma vi x t
          (Just rho) = unify (Just Map.empty) x t'
trpr sys (Out ch msg p pos) gamma h vi ve priv l a =
    (OutClause h (Pred "msg" [ch, msg]) a gamma) : genNames h disclose a gamma ++ trpr sys p gamma h' vi ve priv' l a'
    where a' = relax a priv' l
          disclose = List.nub $ fnp ch ++ fnp msg
          priv' = (priv List.\\ disclose)
          h' = genHyp disclose gamma (systemSets sys) ++ h
trpr sys (New (Var x) (Func TName name []) p pos) gamma h vi ve priv l a =
    trpr sys (subst rho p) gamma' h vi ve (t:priv) l a''
    where a' = zeroVC gamma' (systemSets sys) a t
          a'' = relax a' (t:priv) l
          t = Func TName ("n" ++ posId pos) (vi ++ ve)
          rho = Map.fromList [(Var x, t)]
          gamma' = Map.insert t (Func TName name []) gamma
trpr sys (Let x t g p1 p2 pos) gamma h vi ve priv l a =
    trpr sys p2 gamma h vi ve priv l a' ++
    case g of
      Func TFunc g args -> 
          case List.find (\ r -> reducName r == g && List.length (reducArgs r) == List.length args) (systemReducs sys) of
            Just (Reduc _ args' res' _ _) ->
                case unify (unify (Just Map.empty) (Func TFunc g args) (label (posId pos) $ Func TFunc g args'))
                     x (label (posId pos) res') of
                  Just sigma ->
                      case unifyVC a' sigma of
                        Just a'' ->
                            trpr sys p1' gamma' h' vi' ve' priv l a''
                                where gamma' = substGamma gamma sigma
                                      p1' = subst sigma p1
                                      h' = substList sigma h
                                      vi' = substList sigma vi
                                      ve' = substList sigma ve
                        Nothing -> warning "Set-abstraction unification failed" pos []
                  Nothing -> warning "Destructor unification failed" pos []
            Nothing -> patternMatch
      _ -> patternMatch
    where a' = relax a priv l
          patternMatch =
              case unify (Just Map.empty) x g of
                Just sigma ->
                    case unifyVC a' sigma of
                      Just a'' ->
                          trpr sys p1' gamma' h' vi' ve' priv l a''
                              where gamma' = substGamma gamma sigma
                                    p1' = subst sigma p1
                                    h' = substList sigma h
                                    vi' = substList sigma vi
                                    ve' = substList sigma ve
                      Nothing -> warning "Set-abstraction unification failed" pos []
                Nothing -> warning "Pattern matching failed" pos []
trpr sys (IfE b p1 p2 pos) gamma h vi ve priv l a =
    List.concat $ [trpr sys p1 gamma h vi ve priv l a1 | a1 <- a1s] ++
                  [trpr sys p2 gamma h vi ve priv l a2 | a2 <- a2s]
        where a' = relax a priv l
              a1s = restrict a' b
              a2s = restrict a' (Not b)
trpr sys (SelectE x t (Just b) p pos) gamma h vi ve priv l a =
    List.concat [trpr sys (subst rho p) gamma' (Pred "name" [t'] : h) vi (t' : ve) priv l a1 | a1 <- a1s]
        where a' = relax a priv l
              a1s = restrict a' b
              (gamma', t') = pt gamma vi x t
              (Just rho) = unify (Just Map.empty) x t'
trpr sys (SelectE x t Nothing p pos) gamma h vi ve priv l a =
    trpr sys (subst rho p) gamma' (Pred "name" [t'] : h) vi (t' : ve) priv l a'
        where a' = relax a priv l
              (gamma', t') = pt gamma vi x t
              (Just rho) = unify (Just Map.empty) x t'
trpr sys (Lock ss p pos) gamma h vi ve priv l a =
    trpr sys p gamma h vi ve priv (ss ++ l) (relax a priv l)
trpr sys (Unlock ss p pos) gamma h vi ve priv l a =
    trpr sys p gamma h vi ve priv (l List.\\ ss) (relax a priv l)
trpr sys (SetE b (Event s x NonInjective p pos') pos) gamma h vi ve priv l a =
    trpr sys (Lock [s] (SetE (And b (SetIn x s)) (Unlock [s] p pos') pos) pos) gamma h vi ve priv l a
trpr sys (SetE b (Event s x Injective p pos') pos) gamma h vi ve priv l a =
    trpr sys (Lock [s, s2] (IfE (SetIn x s) (SetE (And b (SetIn x s2)) (Unlock [s, s2] p pos') pos)
                                               (SetE (And b (SetIn x s)) (Unlock [s, s2] p pos') pos)
              pos) pos) gamma h vi ve priv l a
    where Set sname = s
          s2 = Set (sname ++ "_twice")
trpr sys (SetE b p pos) gamma h vi ve priv l a =
    [ImpliesClause h ts' a' a'' gamma | let ts = fnp b ++ fvp b, let ts' = ts List.\\ priv, not(ts' == [])] ++
    trpr sys p gamma h vi ve priv l a''
    where a'  = relax a priv l
          a'' = restrictO a' b
trpr sys (Event s x NonInjective p pos) gamma h vi ve priv l a =
    trpr sys (Lock [s] (SetE (SetIn x s) (Unlock [s] p pos) pos) pos) gamma h vi ve priv l a
trpr sys (Event s x Injective p pos) gamma h vi ve priv l a =
    trpr sys (Lock [s, s2] (IfE (SetIn x s) (SetE (SetIn x s2) (Unlock [s, s2] p pos) pos)
                                               (SetE (SetIn x s) (Unlock [s, s2] p pos) pos)
              pos) pos) gamma h vi ve priv l a
    where Set sname = s
          s2 = Set (sname ++ "_twice")

-- Relaxes the set-abstraction, i.e. removes all constraints on sets
-- that are not locked and names that are not private
relax :: VC -> [Term] -> [Set] -> VC
relax a priv l = [(s, x, y) | (s, x, y) <- a, s `List.elem` l || x `List.elem` priv]

-- Restricts the set-abstraction a, producing all possible refinements
-- of a that satisfy the set expression.
restrict :: VC -> SetExp -> [VC]
restrict a (Not (Not b)) = restrict a b
restrict a (SetIn t s) =
    if (s, repr t, zero) `List.elem` a then []
    else [List.nub $ (s, repr t, one) : a]
restrict a (Not (SetIn t s)) =
    if (s, repr t, one) `List.elem` a then []
    else [List.nub $ (s, repr t, zero) : a]
restrict a (And b1 b2) = List.nub $ List.concat [restrict a' b2 | a' <- restrict a b1]
restrict a (Or b1 b2) = List.union (restrict a b1) (restrict a b2)
restrict a (Not (And b1 b2)) = restrict a (Or (Not b1) (Not b2))
restrict a (Not (Or b1 b2)) = restrict a (And (Not b1) (Not b2))

-- Restricts the set-abstraction, overriding previous restrictions if
-- present.
restrictO :: VC -> SetExp -> VC
restrictO a (Not (Not b)) = restrictO a b
restrictO a (SetIn t s) = (s, r, one) : [(s',r', y) | (s',r',y) <- a, not(s == s' && r == r')]
    where r = repr t
restrictO a (Not (SetIn t s)) = (s, t, zero) : [(s',r', y) | (s',r',y) <- a, not(s == s' && r == r')]
    where r = repr t
restrictO a (And b1 b2) = restrictO (restrictO a b1) b2


-- Generates implies predicates for the translated process
genImplies :: [TrClause] -> [TrClause]
genImplies clauses =
    List.concat $ [genImplies' (ImpliesClause hi ti ai1 ai2 gammai) (OutClause ho co ao gammao) |
                   ImpliesClause hi ti ai1 ai2 gammai <- clauses, (OutClause ho co ao gammao) <- clauses]

genImplies' :: TrClause -> TrClause -> [TrClause]
genImplies' (ImpliesClause h1 ts a1 a2 gamma1) (OutClause _ c _ gamma2) =
    [TImpliesClause ts' c' a1' a2' gamma |
     sigma <- sigmas, let gamma = Map.union (substDom sigma gamma1) (substDom sigma gamma2),
     let c' = subst sigma c, let a1' = substVC a1 sigma, let a2' = substVC a2 sigma,
     let ts' = List.intersect (List.nub $ substList sigma ts) (fnp c' ++ fvp c'), not (ts' == []),
     if impliesOptim then not (predName c' == "name") else True]
    -- Optimization: removing predicates of type "name" since its transitions are already covered by the general rules 
    where sigmas :: [Subst]
          sigmas = if impliesOptim && not (List.length sigma1 == 0) then sigma1 else sigma2
          -- Optimization: the idea is that when the output matches with an input, then the substitution
          -- that matches it can be used instead of any combination.
            where sigma1 = Maybe.catMaybes [unify (Just Map.empty) p c | p <- h1]
                  sigma2 = List.delete (Map.empty) $ List.nub $ mgus_ts ts
          mgus sigma t (Var x) =
              if Map.lookup t gamma1 == Map.lookup (Var x) gamma2 then
                  Maybe.catMaybes [unify (Just sigma) (Var x) t, Just sigma]
              else [sigma]
          mgus sigma t (Func TName n nargs) =
              if Map.lookup t gamma1 == Map.lookup (Func TName n nargs) gamma2 then
                   Maybe.catMaybes [unify (Just sigma) (Func TName n nargs) t, Just sigma]
              else [sigma]
          mgus sigma t (Func _ _ fargs) = List.nub (mgusList sigma t fargs)
          mgus_p sigma t (Pred _ args) = List.nub (mgusList sigma t args)
          mgusList sigma t (arg:args) = List.concat [mgus sigma' t arg | sigma' <- mgusList sigma t args]
          mgusList sigma _ [] = [sigma]
          mgus_ts (t:ts) = List.concat [mgus_p sigma t c | sigma <- sigmas]
              where sigmas = mgus_ts ts
          mgus_ts [] = [Map.empty]

-- Applies a substitution to the domain of a type environment
substDom :: Subst -> Gamma -> Gamma
substDom sigma gamma = Map.mapKeys (subst sigma) gamma

-- Type of a set abstraction
type VC = [(Set, Term, Term)]

-- Typeclass that interprets terms, predicates and clauses using the set-abstraction
class IntVC a where
    intVC :: Gamma -> [SetDecl] -> VC -> a -> a

instance IntVC Clause where
    intVC gamma sets a (Clause cn h c) =
        Clause (List.map (intVC gamma sets a) cn)
               (List.map (intVC gamma sets a) h)
               (intVC gamma sets a c)

instance IntVC Pred where
    intVC gamma sets a (Pred p args) = Pred p (List.map (intVC gamma sets a) args)

instance IntVC Constr where
    intVC gamma sets a (Neq t1 t2) = Neq (intVC gamma sets a t1) (intVC gamma sets a t2)

instance IntVC Term where
    intVC gamma sets a (Var x) =
        Func TFunc ("val_" ++ tname) (Var x:List.map (evalVC a (Var x)) (sets `ofType` tname))
        where (Func TName tname []) = subst gamma (Var x)
    intVC gamma sets a (Func TName n v) =
        Func TFunc ("val_" ++ tname) (Func TName n v:List.map (evalVC a (Func TName n v)) (sets `ofType` tname))
        where (Func TName tname []) = subst gamma (Func TName n v)
    intVC gamma sets a (Func t f args) = Func t f (List.map (intVC gamma sets a) args)

-- Filters declarations of a specific name type
ofType :: [SetDecl] -> String -> [SetDecl]
ofType sets tname = [SetDecl s t e pos | SetDecl s t e pos <- sets, repr t == Func TName tname []]

-- Evaluates the set abstraction for a term into a specific bitstring
evalVC :: VC -> Term -> SetDecl -> Term
evalVC [] x (SetDecl s _ _ _) = Var $ "vc_" ++ s ++ "_" ++ (normalize $ show x)
evalVC ((Set s', x', y):as) x (SetDecl s s_type s_priv pos) =
    if repr x == repr x' && s == s' then y else evalVC as x (SetDecl s s_type s_priv pos)

-- Transforms a possibly complex term into an unique identifier
normalize :: String -> String
normalize ('(':xs) = "__" ++ normalize xs
normalize (')':xs) = "__" ++ normalize xs
normalize (',':xs) = "_" ++ normalize xs
normalize (' ':xs) = normalize xs
normalize ('[':xs) = normalize xs
normalize (']':xs) = normalize xs
normalize (x:xs) = x:normalize xs
normalize [] = []

-- Initializes the value class for a term
zeroVC :: Gamma -> [SetDecl] -> VC -> Term -> VC
zeroVC gamma sets a t = [(Set s, t, zero) | SetDecl s ty _ _ <- sets, repr ty == subst gamma t] ++ a

-- Attempts to apply a substitution to a value class, unifying the variables
unifyVC :: VC -> Subst -> Maybe VC
unifyVC [] _ = Just []
unifyVC ((s, x, y) : a) sigma =
    case a' of
      Just a' ->
          if (s, x', z) `List.elem` a' then Nothing
          else Just $ List.nub ((s, x', y) : a')
      Nothing -> Nothing
    where z = if y == zero then one else zero
          a' = unifyVC a sigma
          x' = subst sigma x

-- Applies a substitution to the domain of a value class, assuming
-- unification always succeeds
substVC :: VC -> Subst -> VC
substVC ((s, t, y):a') sigma = (s, subst sigma t, y): substVC a' sigma
substVC [] _ = []

-- Labels all variables of a term.
label :: String -> Term -> Term
label l (Var x) = Var (x ++ l)
label l (Func tf f args) = Func tf f (List.map (label l) args)

-- Applies a substitution to a type environment
substGamma :: Gamma -> Subst -> Gamma
substGamma gamma sigma = Map.fromList $ substGamma' gamma' sigma
    where gamma' = Map.toList gamma
          substGamma' ((term,type_):gamma) sigma =
              case (subst sigma term, type_) of
                (Var x, Func TName name []) ->
                    (Var x, Func TName name []):(substGamma' gamma sigma)
                (Func TName name args, Func TName name' []) ->
                    (Func TName name args, Func TName name' []):(substGamma' gamma sigma)
                (Func tf1 f1 args1, Func tf2 f2 args2) ->
                    if tf1 == tf2 && f1 == f2 && List.length args1 == List.length args2 then
                        (substGamma' (List.zip args1 args2) sigma) ++ (substGamma' gamma sigma)
                    else error $ "Functions differ " ++ show (Func tf1 f1 args1, Func tf2 f2 args2)
          substGamma' [] sigma = []

-- zero and one constants
zero :: Term
zero = Func TName "c0" []
one :: Term
one = Func TName "c1" []

-- Annotates a type in order to make a pattern that can be used for an
-- hypothesis (e.g. input in the process)
pt :: Subst -> [Term] -> Term -> Term -> (Subst, Term)
pt gamma vi (Var x) (Func TName a []) = (gamma', var)
    where var = Var $ x ++ "_" ++ a ++ "_" ++ (List.intercalate "_" [i | Var i <- vi])
          gamma' = Map.insert var (Func TName a []) gamma
pt gamma _ (Func TName a args) (Func TName a' []) = (gamma, Func TName a args)
pt gamma vi (Var x) (Func tf f args) = (gamma', Func tf f args')
    where (gamma', args') = pt' gamma args 1
          pt' gamma (arg:args) n = (gamma'', arg':args')
              where (gamma', arg') = pt gamma (Var (show n) : vi) (Var x) arg
                    (gamma'', args') = pt' gamma' args (n+1)
          pt' gamma [] n = (gamma, [])
pt gamma vi (Func TFunc f1 args1) (Func TFunc f2 args2) =
    if f1 == f2 && List.length args1 == List.length args2 then (gamma', Func TFunc f1 args')
    else error $ "pt: Mismatch between " ++ show (Func TFunc f1 args1) ++ " " ++ show (Func TFunc f2 args2)
    where (gamma', args') = pt'' gamma vi args1 args2 1
pt gamma vi (Func TTuple _ args1) (Func TTuple f args2) = (gamma', Func TTuple f args')
    where (gamma', args') = pt'' gamma vi args1 args2 1
pt gamma vi a b = error $ "pt: Pattern not expected " ++ show a ++ " " ++ show b

pt'' gamma vi (a1:args1) (a2:args2) n = (gamma'', a':args')
    where (gamma', a') = pt gamma (Var (show n) : vi) a1 a2
          (gamma'', args') = pt'' gamma' vi args1 args2 (n + 1)
pt'' gamma vi [] [] n = (gamma, [])
