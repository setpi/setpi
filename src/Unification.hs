{-# LANGUAGE DeriveDataTypeable,TypeSynonymInstances,FlexibleInstances #-}
module Unification (unify,unifyList,subsumes,subsumesCheck,subst,substList,fv,fvp,fnp,fvList,fvpList,fnpList,tupleSizesP) where --,stdApart,stdApart',stdApartList) where

import Types

import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.Maybe as Maybe
import Control.Monad.State
import Control.Exception



class Unifiable a where
    unify :: (Maybe Subst) -> a -> a -> (Maybe Subst)

instance Unifiable Term where
    unify (Just rho) (Var x) (Var y) =
        if x == y then (Just rho)
        else case Map.lookup (Var x) rho of
               Just t -> unify (Just rho) (Var y) t
               Nothing -> Just $ Map.insert (Var x) (Var y) rho
    unify (Just rho) (Var x) (Func ty f fArgs) =
        case Map.lookup (Var x) rho of
          Just t -> unify (Just rho) t (Func ty f fArgs)
          Nothing -> if occurs (Var x) (Func ty f fArgs) then Nothing
                     else Just $ Map.insert (Var x) (Func ty f fArgs) rho
    unify (Just rho) (Func ty f fArgs) (Var x) =
        unify (Just rho) (Var x) (Func ty f fArgs)
    unify (Just rho) (Func fTy f fArgs) (Func gTy g gArgs) =
        if f == g && fTy == gTy then unifyList (Just rho) fArgs gArgs
        else Nothing
    unify Nothing _ _ = Nothing

occurs :: Term -> Term -> Bool
occurs (Var x) (Var y) = x == y
occurs (Var x) (Func ty f fArgs) = List.any (occurs (Var x)) fArgs
occurs t1 t2 = error $ "Occurs takes a variable as first argument, got " ++ show t1 ++ " and " ++ show t2 ++ " instead."

unifyList :: Unifiable a => Maybe Subst -> [a] -> [a] -> Maybe Subst
unifyList (Just rho) (x:xs) (y:ys) =
    unifyList (unify (Just rho) x y) xs ys
unifyList (Just rho) [] [] = Just rho
unifyList (Just rho) _ _ = Nothing
unifyList Nothing _ _ = Nothing

class Substitutable a where
    subst :: Subst -> a -> a

instance Substitutable Term where
    subst sigma (Var x) =
        case Map.lookup (Var x) sigma of
          Nothing -> (Var x)
          Just t -> if Var x == t then t else subst sigma t
    subst sigma (Func ty f args) =
        case Map.lookup (Func ty f args) sigma of
          Just t -> if Func ty f args == t then t else subst sigma t
          Nothing -> Func ty f (substList sigma args)

instance Substitutable Pred where
    subst sigma (Pred p args) = Pred p (substList sigma args)

instance Substitutable Constr where
    subst sigma (Neq t1 t2) = Neq (subst sigma t1) (subst sigma t2)

instance Substitutable Clause where
    subst sigma (Clause cn h c) = Clause (substList sigma cn) (substList sigma h) (subst sigma c)

instance Substitutable Process where
    subst sigma Nil = Nil
    subst sigma (Par p1 p2 pos) = Par (subst sigma p1) (subst sigma p2) pos
    subst sigma (Repl p pos) = Repl (subst sigma p) pos
    subst sigma (New x t p pos) = New (subst sigma x) (subst sigma t) (subst sigma p) pos
    subst sigma (In ch x t p pos) = In (subst sigma ch) (subst sigma x) (subst sigma t) (subst sigma p) pos
    subst sigma (Out ch msg p pos) = Out (subst sigma ch) (subst sigma msg) (subst sigma p) pos
    subst sigma (Let x t f p1 p2 pos) = Let (subst sigma x) (subst sigma t) (subst sigma f) (subst sigma p1) (subst sigma p2) pos
    subst sigma (IfE b p1 p2 pos) = IfE (subst sigma b) (subst sigma p1) (subst sigma p2) pos
    subst sigma (SetE b p pos) = SetE (subst sigma b) (subst sigma p) pos
    subst sigma (SelectE x t b p pos) = SelectE (subst sigma x) (subst sigma t) (subst sigma b) (subst sigma p) pos
    subst sigma (Event s x ty p pos) = Event s (subst sigma x) ty (subst sigma p) pos
    subst sigma (Lock s p pos) = Lock s (subst sigma p) pos
    subst sigma (Unlock s p pos) = Unlock s (subst sigma p) pos

instance Substitutable SetExp where
    subst sigma (SetIn t s) = SetIn (subst sigma t) s
    subst sigma (And e1 e2) = And (subst sigma e1) (subst sigma e2)
    subst sigma (Or e1 e2) = Or (subst sigma e1) (subst sigma e2)
    subst sigma (Not e) = Not (subst sigma e)

instance (Substitutable a) => Substitutable (Maybe a) where
    subst sigma (Just x) = Just (subst sigma x)
    subst sigma (Nothing) = Nothing

substList :: Substitutable a => Subst -> [a] -> [a]
substList sigma = List.map (subst sigma)

class Subsumable a where
    subsumes :: a -> a -> Bool
    subsumesCheck :: a -> a -> Maybe Subst -> Maybe Subst

instance Subsumable Term where
    subsumes x y = case subsumesCheck x y (Just Map.empty) of
                     Just rho -> True
                     Nothing -> False
    subsumesCheck (Var name1) y rho = unify rho (Var name1) y
    subsumesCheck (Func tf1 f1 args1) (Func tf2 f2 args2) rho =
        if tf1 == tf2 && f1 == f2 then subsumesList args1 args2 rho else Nothing
    subsumesCheck (Func tf1 f1 args1) y rho = Nothing

subsumesList (x:xs) (y:ys) rho = subsumesList xs ys rho'
    where rho' = subsumesCheck x y rho
subsumesList [] [] rho = rho
subsumesList xs ys rho =
    error $ "Unifying lists " ++ show xs ++ " and " ++ show ys ++ " with different lengths"

instance Unifiable Pred where
    unify rho (Pred p1 pargs1) (Pred p2 pargs2) =
        if p1 == p2 then
            List.foldl foldUnify rho $ List.zip pargs1 pargs2
        else Nothing
        where foldUnify rho (x,y) = unify rho x y

instance Subsumable Pred where
    subsumes p1 p2 = case subsumesCheck p1 p2 (Just Map.empty) of
                       Just rho -> True
                       Nothing -> False

    subsumesCheck (Pred p1 pargs1) (Pred p2 pargs2) rho =
        if p1 == p2 then subsumesList pargs1 pargs2 rho else Nothing

instance Subsumable Clause where
    subsumes (Clause cn1 h1 c1) (Clause cn2 h2 c2) =
        if List.length h1 <= List.length h2 then
            not $ List.null $ Maybe.catMaybes $ findAll h1 h2 [rho]
        else False
        where rho = subsumesCheck c1 c2 (Just Map.empty)
              findOne p1 (p2:h2) rho =
                  case subsumesCheck p2 p1 rho of
                    Just rho' -> Just rho' : findOne p1 h2 rho
                    Nothing -> findOne p1 h2 rho
              findOne p1 [] rho = []
              findAll (p1:h1) h2 rhos =
                  case h1 of
                    [] -> rhos'
                    _ -> findAll h1 h2 rhos'
                  where rhos' = List.concatMap (findOne p1 h2) rhos
              findAll [] h2 rhos = rhos
    subsumesCheck _ _ _ = error "subsumesCheck unapplicable to clauses" 

class FV a where
    fv :: a -> [Term]

instance FV Term where
    fv (Var x) = [Var x]
    fv (Func ty f args) = fvList args

fvList :: FV a => [a] -> [Term]
fvList l = List.nub $ List.concatMap fv l

class FVProc a where
    fvp :: a -> [Term]
    fnp :: a -> [Term]

fvpList :: FVProc a => [a] -> [Term]
fvpList l = List.nub $ List.concatMap fvp l
fnpList :: FVProc a => [a] -> [Term]
fnpList l = List.nub $ List.concatMap fnp l

instance FVProc Term where
    fvp (Var x) = [Var x]
    fvp (Func TName name nargs) = []
    fvp (Func ty f args) = fvpList args
    fnp (Var x) = []
    fnp (Func TName name nargs) = [Func TName name nargs]
    fnp (Func tf f fargs) = fnpList fargs

instance FVProc Pred where
    fvp (Pred n args) = fvpList args
    fnp (Pred n args) = fnpList args

instance FVProc Process where
    fvp (Nil) = []
    fvp (Par p1 p2 pos) = List.nub $ fvp p1 ++ fvp p2
    fvp (Repl p pos) = fvp p
    fvp (New (Var x) t p pos) = List.delete (Var x) (fvp p)
    fvp (New x t p pos) = error "New should have a variable as first parameter"
    fvp (In ch x t p pos) = (List.nub $ fvp ch ++ fvp p) List.\\ (fvp x ++ fvp t)
    fvp (Out ch msg p pos) = List.nub $ fvp ch ++ fvp msg ++ fvp p
    fvp (Let t1 ty1 t2 p1 p2 pos) = List.nub (fvp t2 ++ fvp p1 ++ fvp p2) List.\\ (fvp t1 ++ fvp ty1)
    fvp (IfE b p1 p2 pos) = List.nub $ fvp b ++ fvp p1 ++ fvp p2
    fvp (SetE b p pos) = List.nub $ fvp b ++ fvp p
    fvp (SelectE x t b p pos) = (List.nub $ (case b of Just b -> fvp b; Nothing -> []) ++ fvp p) List.\\ (fvp x ++ fvp t)
    fvp (Event s x ty p pos) = List.nub $ fvp x ++ fvp p
    fvp (Lock s p pos) = fvp p
    fvp (Unlock s p pos) = fvp p
    fnp p = error "fnp: Not implemented"

instance FVProc SetExp where
    fvp (SetIn t s) = fvp t
    fvp (And e1 e2) = List.nub $ fvp e1 ++ fvp e2
    fvp (Or e1 e2) = List.nub $ fvp e1 ++ fvp e2
    fvp (Not e) = fvp e
    fnp (SetIn t s) = fnp t
    fnp (And e1 e2) = List.nub $ fnp e1 ++ fnp e2
    fnp (Or e1 e2) = List.nub $ fnp e1 ++ fnp e2
    fnp (Not e) = fnp e

tupleSizesP (Nil) = []
tupleSizesP (Par p1 p2 _) = List.nub $ tupleSizesP p1 ++ tupleSizesP p2
tupleSizesP (Repl p _) = tupleSizesP p
tupleSizesP (New _ _ p _) = tupleSizesP p
tupleSizesP (In t1 _ t2 p _) = List.nub $ tupleSizes t1 ++ tupleSizes t2 ++ tupleSizesP p
tupleSizesP (Out t1 t2 p _) = List.nub $ tupleSizes t1 ++ tupleSizes t2 ++ tupleSizesP p
tupleSizesP (Let t1 ty1 t2 p1 p2 _) = List.nub $ tupleSizes t1 ++ tupleSizes t2 ++ tupleSizesP p1 ++ tupleSizesP p2
tupleSizesP (IfE _ p1 p2 _) = List.nub $ tupleSizesP p1 ++ tupleSizesP p2
tupleSizesP (SetE b p _) = tupleSizesP p
tupleSizesP (SelectE x t b p pos) = tupleSizesP p
tupleSizesP (Event _ _ _ p _) = tupleSizesP p
tupleSizesP (Lock _ p _) = tupleSizesP p
tupleSizesP (Unlock _ p _) = tupleSizesP p

tupleSizes (Var _) = []
tupleSizes (Func TName _ _) = []
tupleSizes (Func TFunc _ args) = List.nub $ List.concatMap tupleSizes args
tupleSizes (Func TTuple _ args) = List.nub $ List.length args : List.concatMap tupleSizes args
