module ProVerifOut where

import Types
import qualified Data.List as List

outProVerif :: PrologProgram -> String
outProVerif p = (showSep "\n" $ prologPredDecls p) ++ "\n\n" ++ 
                (List.intercalate "\n" $ map (\x -> "nounif " ++ show x ++ ".") (prologNoUnif p)) ++ "\n\n" ++
                (showSep "\n" $ prologFuncDecls p) ++ "\n\n" ++
                (List.intercalate "\n" $ map (\x -> "query " ++ show x ++ ".") (prologQueries p)) ++ "\n\nreduc\n" ++
                (showSep ";\n" $ prologClauses p) ++ "."
