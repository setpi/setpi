{-# LANGUAGE DeriveDataTypeable,DoAndIfThenElse #-}
module ParsePi (parsePi) where

import Types
import Unification

import qualified Text.ParserCombinators.Parsec.Token as T
import Text.ParserCombinators.Parsec
import qualified Text.Parsec as Parsec
import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.Maybe as Maybe

data MacroDef = MacroDef { macroName :: String, macroArgs :: [Term], macroProc :: Process }
                deriving (Show,Eq)

piLang :: T.LanguageDef st
piLang = T.LanguageDef { T.commentStart = "(*",
                         T.commentEnd = "*)",
                         T.commentLine = "",
                         T.nestedComments = True,
                         T.identStart = letter <|> oneOf "_",
                         T.identLetter = alphaNum <|> oneOf "_'",
                         T.opStart = oneOf ":!#$%&*+./<=>?@\\^|-~",
                         T.opLetter = oneOf "",
                         T.reservedNames = ["agree", "fun", "pred", "reduc", "set",
                                            "forall", "in", "out", "let", "if", "inj-agree",
                                            "inj-event", "then", "else", "enter", "event", "exit",
                                            "new", "and", "or", "private", "update",
                                            "query", "select", "where", "satisfying"],
                         T.reservedOpNames = [],
                         T.caseSensitive = True }

lexer = T.makeTokenParser piLang

term = try func <|> try tuple <|> var
typeTerm = try func <|> try tuple <|> var

var = do
  name <- identifier
  return (Var name)

func = do
  name <- identifier; args <- parens (commaSep term)
  return (Func TFunc name args)

tuple = do
  args <- parens (commaSep term)
  return (mkTuple args)

pred = do
  name <- identifier; args <- parens (commaSep term)
  return (Pred name args)

pattern = try tuplePattern <|> try varBind <|> try match

tuplePattern = do
  tmp <- parens (commaSep pattern)
  let (pts, tys) = unzip tmp
  return (mkTuple pts, mkTuple tys)

varBind = do
  pt <- var
  colon
  ty <- term
  return (pt, ty)

match = do
  symbol "="
  m <- term
  types
  t <- term
  return (m, t)

process :: GenParser Char Decl Process
process = do
  p1 <- (    input       <|> output
         <|> lockSet     <|> unlockSet
         <|> event       <|> injevent
         <|> setTransition <|> select
         <|> replication <|> restriction 
         <|> letBinder   <|> ifThenElse
         <|> macroCall   <|> (parens ParsePi.process))
  try (do pos <- getPosition; symbol "|"; p2 <- ParsePi.process; return (Par p1 p2 pos))
          <|> return p1

cont :: GenParser Char Decl Process
cont = (semi >> (ParsePi.process)) <|> return Nil

input :: GenParser Char Decl Process
input = do
  pos <- getPosition
  reserved "in"; symbol "("; ch <- term; comma; (pt, ty) <- pattern; symbol ")"; cont <- cont
  return (In ch pt ty cont pos)

output :: GenParser Char Decl Process
output = do
  pos <- getPosition
  reserved "out"; symbol "("; ch <- term; comma; ms <- term; symbol ")"; p <- cont
  return (Out ch ms p pos)

replication :: GenParser Char Decl Process
replication = do
  pos <- getPosition
  bang; ss <- lockedSets; p <- ParsePi.process
  return (Repl (lockProcess pos ss $ unlockProcess pos ss p) pos)

lockedSets :: GenParser Char Decl [Set]
lockedSets = do
  try (do symbol "{"; ss <- commaSep set; symbol "}"; return ss) <|> return []

restriction :: GenParser Char Decl Process
restriction = do
  pos <- getPosition
  reserved "new"; x <- var; colon; t <- var; p <- cont
  return (New x t p pos)

letBinder :: GenParser Char Decl Process
letBinder = do
  pos <- getPosition
  reserved "let"; (x, t) <- pattern; eq; g <- term; reserved "in"; p1 <- ParsePi.process
  (do reserved "else"; p2 <- ParsePi.process; return (Let x t g p1 p2 pos)) <|> (do return (Let x t g p1 Nil pos))

ifThenElse :: GenParser Char Decl Process
ifThenElse = do
  pos <- getPosition
  reserved "if"; b <- boolExp; reserved "then"; p1 <- ParsePi.process
  (do reserved "else"; p2 <- ParsePi.process; return (IfE b p1 p2 pos)) <|> (do return (IfE b p1 Nil pos))

setTransition :: GenParser Char Decl Process
setTransition = do
  pos <- getPosition
  try (reserved "update") <|> reserved "set"; b <- boolExp; p <- cont
  -- Allow both update and set as keywords (transition to update)
  return (SetE b p pos)

select :: GenParser Char Decl Process
select = do
  pos <- getPosition
  reserved "select"; x <- var; colon; t <- term;
  b <- try (do reserved "where"; b <- boolExp; return (Just b)) <|> return Nothing;
  p <- cont
  return (SelectE x t b p pos)

lockSet :: GenParser Char Decl Process
lockSet = do
  pos <- getPosition
  reserved "lock"; ss <- parens (commaSep set); p <- cont
  return (Lock ss p pos)

unlockSet :: GenParser Char Decl Process
unlockSet = do
  pos <- getPosition
  reserved "unlock"; ss <- parens (commaSep set); p <- cont
  return (Unlock ss p pos)

applyF :: (Set -> Process -> Process) -> [Set] -> Process -> Process
applyF f (s:ss) p = f s $ applyF f ss p
applyF _ [] p = p

event :: GenParser Char Decl Process
event = do
  pos <- getPosition
  reserved "event"; name <- identifier; x <- parens (term); p <- cont
  return (Event (Set name) x NonInjective p pos)
--  return (Lock (Set name) (SetE (SetIn x (Set name)) (Unlock (Set name) p pos) pos) pos)

injevent :: GenParser Char Decl Process
injevent = do
  pos <- getPosition
  reserved "inj-event"; name <- identifier; x <- parens (term); p <- cont
  return (Event (Set name) x Injective p pos)
  -- let name_twice = name ++ "_twice"
  -- return (Lock (Set name) 
  --         (Lock (Set name_twice) 
  --          (IfE (Not (SetIn x (Set name)))
  --              (SetE (SetIn x (Set name)) (Unlock (Set name) (Unlock (Set name_twice) p pos) pos) pos)
  --              (SetE (SetIn x (Set name_twice)) (Unlock (Set name) (Unlock (Set name_twice) p pos) pos) pos)
  --           pos) pos) pos)

set :: GenParser Char Decl Set
set = do
  s <- identifier
  return (Set s)

boolExp :: GenParser Char Decl SetExp
boolExp = orExp

setExp :: GenParser Char Decl SetExp
setExp = do
  t <- term; reserved "in"; s <- set
  return (SetIn t s)

notExp :: GenParser Char Decl SetExp
notExp =
  (do reserved "not"; e <- try setExp <|> parens boolExp; return (Not e)) <|>
  (try setExp <|> parens boolExp) 

andExp :: GenParser Char Decl SetExp
andExp = do
  e1 <- notExp
  try (do reserved "and"; e2 <- boolExp; return (And e1 e2)) <|> return e1

orExp :: GenParser Char Decl SetExp
orExp = do
  e1 <- andExp
  try (do reserved "or"; e2 <- boolExp; return (Or e1 e2)) <|> return e1


data Decl = Decl { declMacro :: [MacroDef],
                   declType :: [(Term, Term)],
                   declSet :: [SetDecl],
                   declFun :: [FuncDecl],
                   declReduc :: [Reduc],
                   declQuery :: [Query],
                   declTerm :: (Process, Process) }

emptyDecl :: Decl
emptyDecl = Decl [] [] [] [] [] [] (Nil, Nil)

decl :: GenParser Char Decl ()
decl = do
  try typeDecl <|> try setDecl <|> try eventDecl <|> injeventDecl <|> try funDecl <|> try reducDecl <|>
      try agreeQueryDef <|> try injAgreeQueryDef <|> try queryDef <|> try termDecl <|> macroDef

macroCall :: GenParser Char Decl Process
macroCall = do
  pos <- getPosition
  name <- identifier; args <- parens (commaSep term)
  state <- getState
  let macros = declMacro state
  case List.find (\m-> macroName m == name) macros of
    Just (MacroDef _ args' p) -> 
        if List.length args == List.length args' then
            return $ subst (Map.fromList $ List.zip args' args) p
        else error $ "Different number of arguments for macro call " ++ name ++ " at " ++ show pos
    Nothing -> error $ "No macro found with name " ++ name ++ " at " ++ show pos

paramDef :: GenParser Char Decl Param
paramDef = do
  reserved "param"; key <- identifier; symbol "="; value <- identifier; dot
  return (PVParam key value)

piProgram :: GenParser Char Decl System
piProgram = do
  whiteSpace
  params <- many paramDef
  many decl
  reserved "process"
  p <- ParsePi.process
  eof
  Decl macros types sets funcs reducs queries (tpriv, tpub) <- getState
  let gamma = Map.fromList types
  let tpriv' = subst gamma tpriv
  let tpub' = subst gamma tpub
  let p' = subst gamma p
  let termMap = (Map.union (procToMap tpriv') (procToMap tpub'))
  let typeMap = (Map.union (procToTyMap tpriv') (procToTyMap tpub'))
  let queries' = List.map (substQ gamma termMap typeMap) queries
  let sets' = List.map (substS gamma) sets
  let funcs' = List.map (substF gamma) funcs
  let types' = [Func TName tname [] | (_, Func TName tname []) <- types]
  return (System (restrictProcess tpriv' p') tpub' reducs sets' queries' funcs' types' params)

substF :: Gamma -> FuncDecl -> FuncDecl
substF gamma (FuncDecl f targs b pos) = (FuncDecl f (List.map (subst gamma) targs) b pos)

posId :: SourcePos -> String
posId pos = show (sourceLine pos) ++ "x" ++ show (sourceColumn pos)

procToMap :: Process -> Subst
procToMap (New (Var x) (Func TName tname []) p pos) =
    Map.insert (Var x) (Func TName ("n" ++ posId pos ) []) (procToMap p)
procToMap Nil = Map.empty
procToTyMap :: Process -> Subst
procToTyMap (New (Var x) (Func TName tname []) p pos) =
    Map.insert (Func TName ("n" ++ posId pos ) []) (Func TName tname []) (procToTyMap p)
procToTyMap Nil = Map.empty

substQ :: Gamma -> Subst -> Gamma -> Query -> Query
substQ tenv tMap tyMap (Query p b gamma) = Query p' b' gamma''
    where gamma'' = Map.union (Map.fromList gamma') tyMap
          (sigma, gamma') = substQ' $ Map.toList gamma
          substQ' ((Var x, t):gamma) = ((Var x, v):sigma, g++gs)
              where (v, g) = pt [] (Var x) (subst tenv t)
                    (sigma, gs) = substQ' gamma
          substQ' [] = ([], [])
          pt :: [Int] -> Term -> Term -> (Term, [(Term, Term)])
          pt ns (Var x) (Func TName name []) = (v, [(v, Func TName name [])])
              where v = Var (x ++ (List.intercalate "_" $ List.map show ns))
          pt ns (Var x) (Func tf f fargs) = (Func tf f fargs', gamma')
              where pt' n (arg:args) = (v:vs, gamma ++ gammas) 
                        where (v, gamma) = pt (n:ns) (Var x) arg
                              (vs, gammas) = pt' (n+1) args
                    pt' n [] = ([], [])
                    (fargs', gamma') = pt' 1 fargs
          sigma' = Map.union (Map.fromList sigma) tMap
          b' = case b of Just b -> Just (subst sigma' b); Nothing -> Nothing
          p' = subst sigma' p

substS :: Gamma -> SetDecl -> SetDecl
substS gamma (SetDecl s t e pos) = SetDecl s (subst gamma t) e pos

typeDecl :: GenParser Char Decl ()
typeDecl = do
  reserved "type"; name <- identifier
  try (do { symbol "="; t <- typeTerm; dot; addType (Var name) t }) <|>
      (do { dot; addType (Var name) (Func TName name []) })

addType name type_ = do
  Decl m t s f r q te <- getState
  setState (Decl m ((name,type_):t) s f r q te) 

funDecl :: GenParser Char Decl ()
funDecl = do
  pos <- getPosition
  reserved "fun"; name <- identifier; targs <- parens (commaSep term)
  let f private = addFuncDecl $ FuncDecl name targs private pos
  try (do { brackets (reserved "private"); dot; f True }) <|>
          (do { dot; f False })

addFuncDecl fd = do
  Decl m t s f r q te <- getState
  setState (Decl m t s (fd:f) r q te)

reducDecl :: GenParser Char Decl ()
reducDecl = do
  pos <- getPosition
  reserved "reduc"
  name <- identifier; params <- parens $ commaSep term; eq; result <- term
  let f private = addReduc $ Reduc name params result private pos
  try (do { brackets (reserved "private"); dot; f True }) <|>
          (do { dot; f False })

addReduc red = do
  Decl m t s f r q te <- getState
  setState (Decl m t s f (red:r) q te)

setDecl :: GenParser Char Decl ()
setDecl = do
  pos <- getPosition
  reserved "set"; name <- identifier; colon; t <- term; dot
  addSet $ SetDecl name t SetDeclT pos

eventDecl :: GenParser Char Decl ()
eventDecl = do
  pos <- getPosition
  reserved "event"; name <- identifier; t <- parens (term); dot
  addSet $ SetDecl name t EventDeclT pos

injeventDecl :: GenParser Char Decl ()
injeventDecl = do
  pos <- getPosition
  reserved "inj-event"; name <- identifier; t <- parens (term); dot
  addSet $ SetDecl name t EventDeclT pos
  addSet $ SetDecl (name ++ "_twice") t InjEventDeclT pos

addSet set = do
  Decl m t s f r q te <- getState
  setState (Decl m t (set:s) f r q te)

typeEnv = do
  type_ <- commaSep (do x <- var; colon; t <- term; return (x, t))
  return (Map.fromList type_)

agreeQueryDef :: GenParser Char Decl ()
agreeQueryDef = do
  pos <- getPosition
  reserved "query"; gamma <- typeEnv; semi; reserved "agree"; Func TFunc e1 [t1] <- term; implies; Func TFunc e2 [t2] <- term;
  if repr t1 == repr t2 then return () else error $ "Expected same variable in event at " ++ show pos
  let Var x1 = repr t1
  dot
  addQuery $ Query (Pred "name" [Var x1]) (Just $ And (Not (SetIn t2 (Set e2))) (SetIn t1 (Set e1))) gamma

injAgreeQueryDef :: GenParser Char Decl ()
injAgreeQueryDef = do
  pos <- getPosition
  reserved "query"; gamma <- typeEnv; semi; reserved "inj-agree"; Func TFunc e1 [t1] <- term; implies; Func TFunc e2 [t2] <- term;
  if repr t1 == repr t2 then return () else error $ "Expected same variable in event at " ++ show pos
  let Var x1 = repr t1
  dot
  addQuery $ Query (Pred "name" [Var x1]) (Just $ Or (And (Not (SetIn t2 (Set e2))) (SetIn t1 (Set e1))) (SetIn t1 (Set $ e1++"_twice"))) gamma

queryDef :: GenParser Char Decl ()
queryDef = do
  reserved "query"; gamma <- typeEnv; semi; pred <- ParsePi.pred;
  constraints <- try (do reserved "where"; b <- boolExp; return $ Just b) <|> do return Nothing
  dot
  addQuery $ Query pred constraints gamma

termDecl :: GenParser Char Decl ()
termDecl = do
  pos <- getPosition
  reserved "new"; name <- identifier; colon; type_ <- var
  private <- try (do brackets (reserved "private"); dot; return True) <|> (do dot; return False)
  Decl m t s f r q (tpriv, tpub) <- getState
  if private then
      setState (Decl m t s f r q (New (Var name) type_ tpriv pos, tpub))
  else
      setState (Decl m t s f r q (tpriv, New (Var name) type_ tpub pos))

macroDef :: GenParser Char Decl ()
macroDef = do
  reserved "let"; name <- identifier; args <- optionMaybe (parens (commaSep var))
  let args' = Maybe.fromMaybe [] args
  eq; p <- ParsePi.process; dot
  addMacro (MacroDef name args' p)

addMacro macro = do
  Decl m t s f r q te <- getState
  setState $ Decl (macro:m) t s f r q te

addQuery query = do
  Decl m t s f r q te <- getState
  setState $ Decl m t s f r (query:q) te

identifier = T.identifier lexer
parens = T.parens lexer
brackets = T.brackets lexer
commaSep = T.commaSep lexer
colon = T.colon lexer
reserved = T.reserved lexer
reservedOp = T.reserved lexer
comma = T.comma lexer
symbol = T.symbol lexer
semi = T.semi lexer
whiteSpace = T.whiteSpace lexer
natural = T.natural lexer

bang = symbol "!"
eq = reservedOp "="
neq = reservedOp "<>"
dot = symbol "."
types = symbol ":"
implies = reservedOp "==>"

parsePi :: String -> IO (Either ParseError System)
parsePi file = do
  input <- readFile file
  return $ Parsec.runParser piProgram emptyDecl file input

-- Helper functions
unlockProcess :: SourcePos -> [Set] -> Process -> Process
unlockProcess pos' [] Nil = Nil
unlockProcess pos' ss Nil = Unlock ss Nil pos'
unlockProcess pos' ss (In ch x t cont pos) = In ch x t (unlockProcess pos' ss cont) pos
unlockProcess pos' ss (Out ch m cont pos) = Out ch m (unlockProcess pos' ss cont) pos
unlockProcess pos' ss (Repl p pos) = Repl (unlockProcess pos' ss p) pos
unlockProcess pos' ss (Par p1 p2 pos) = Par (unlockProcess pos' ss p1) (unlockProcess pos' ss p2) pos
unlockProcess pos' ss (New x t cont pos) = New x t (unlockProcess pos' ss cont) pos
unlockProcess pos' ss (Let t1 ty1 t2 p1 p2 pos) = Let t1 ty1 t2 (unlockProcess pos' ss p1) (unlockProcess pos' ss p2) pos
unlockProcess pos' ss (IfE b p1 p2 pos) = IfE b (unlockProcess pos' ss p1) (unlockProcess pos' ss p2) pos
unlockProcess pos' ss (SetE b cont pos) = SetE b (unlockProcess pos' ss cont) pos
unlockProcess pos' ss (SelectE x t b cont pos) = SelectE x t b (unlockProcess pos' ss cont) pos
unlockProcess pos' ss (Event x t ty cont pos) = Event x t ty (unlockProcess pos' ss cont) pos
unlockProcess pos' ss (Lock s cont pos) = Lock s (unlockProcess pos' ss cont) pos
unlockProcess pos' ss (Unlock s cont pos) = Unlock s (unlockProcess pos' ss cont) pos

lockProcess :: SourcePos -> [Set] -> Process -> Process
lockProcess pos [] p = p
lockProcess pos ss p = Lock ss p pos

