module Types where

import qualified Data.Map as Map
import qualified Data.List as List
import qualified Text.ParserCombinators.Parsec.Pos as Pos
import Control.Monad.State
import Text.Parsec.Pos
import Debug.Trace

data Term = Var { varName :: String }
          | Func { funcType :: FuncType, funcName :: String, funcArgs :: [Term]}
            deriving (Ord, Eq)

data FuncType = TFunc | TTuple | TName
                deriving (Ord, Eq)

mkTuple args = Func TTuple ("mktuple_" ++ (show $ List.length args)) args

showSep sep args = (List.intercalate sep $ List.map show args)

repr :: Term -> Term
repr (Var x) = Var x
repr (Func TName name args) = Func TName name args
repr (Func TFunc name [arg]) = repr arg
repr (Func TFunc name args) = error ("The function `" ++ show (Func TFunc name args) ++ "' is not monadic.")
repr (Func TTuple _ args) = error "No representative in a tuple"

instance Show Term where
    show (Var n) = n
    show (Func TFunc f args) = f ++ "(" ++ showSep "," args ++ ")"
    show (Func TTuple f args) = "(" ++ showSep "," args ++ ")"
    show (Func TName f args) = f ++ "[" ++ showSep "," args ++ "]"

data Pred = Pred { predName :: String, predArgs :: [Term] }
            deriving (Eq)

data Constr = Neq { neqT1 :: Term, neqT2 :: Term }
              deriving (Eq)

instance Show Pred where
    show (Pred name args) = name ++ ":" ++ showSep "," args ++ ""

instance Show Constr where
    show (Neq t1 t2) = show t1 ++ "<>" ++ show t2

data Clause = Clause { clauseConstraints :: [Constr], clauseHypothesis :: [Pred], clauseConclusion :: Pred }
              deriving (Eq)

instance Show Clause where
    show (Clause [] [] c) = show c
    show (Clause cn h c) = (List.intercalate " & " $ List.map show cn) ++ 
                           (List.intercalate " & " $ List.map show h) ++ " -> " ++ show c

data Process = Nil
             | Par { procPar1 :: Process, procPar2 :: Process, procParPos :: SourcePos }
             | Repl { procRepl :: Process, procReplPos :: SourcePos }
             | New { procNewVar :: Term, procNewType :: Term, procNewCont :: Process, procNewPos :: SourcePos }
             | In { procInCh :: Term, procInBind :: Term, procInType :: Term, procInCont :: Process, procInPos :: SourcePos }
             | Out { procOutCh :: Term, procOutMsg :: Term, procOutCont :: Process, procOutPos :: SourcePos }
             | Let { procLetPattern :: Term, procLetPatternType :: Term, procLetMatch :: Term,
                     procLetThen :: Process, procLetElse :: Process, procLetPos :: SourcePos }
             | IfE { procIfExp :: SetExp, procIfThen :: Process, procIfElse :: Process, procIfPos :: SourcePos }
             | SetE { procSetExp :: SetExp, procEnterCont :: Process, procEnterPos :: SourcePos }
             | SelectE { procSelVar :: Term, procSelType :: Term, procSelExp :: Maybe SetExp, procSelCont :: Process, procSelPos :: SourcePos }
             | Event { procEventName :: Set, procEventTerm :: Term, procEventType :: EventType, procEventCont :: Process, procEventPos :: SourcePos }
             | Lock { procLockSet :: [Set], procLockCont :: Process, procLockPos :: SourcePos }
             | Unlock { procUnlockSet :: [Set], procUnlockCont :: Process, procUnlockPos :: SourcePos }
               deriving (Eq)

data EventType = NonInjective | Injective
               deriving (Eq, Show)

instance Show Process where
    show (Nil) = ""
    show (Par p1 p2 pos) = "(" ++ (indent $ show p1) ++ ") | (" ++ (indent $ show p2) ++ ")"
    show (Repl p pos) = "!\n" ++ show p
    show (New x t p pos) = "new " ++ show x ++ " : " ++ show t ++ ";\n" ++ show p
    show (In ch x t p pos) = "in(" ++ show ch ++ ", " ++ show x ++ ":" ++ show t ++ ");\n" ++ show p
    show (Out ch msg p pos) = "out(" ++ show ch ++ ", " ++ show msg ++ ");\n" ++ show p
    show (Let t1 ty1 t2 p1 p2 pos) = "let " ++ show t1 ++ ": " ++ show ty1 ++ " = " ++ show t2 ++ " in\n"
                                 ++ (indent $ show p1) ++ "else\n" ++ (indent $ show p2)
    show (IfE b p1 p2 pos) = "if " ++ show b ++ " then\n" ++ (indent $ show p1) ++ " else\n" ++ (indent $ show p2)
    show (SetE b p pos) = "set " ++ show b ++ ";\n" ++ show p
    show (SelectE x t b p pos) = "select " ++ show x ++ " : " ++ show t ++
                                 (case b of Just b -> " where " ++ show b; Nothing -> "") ++ " in " ++ show p
    show (Event e x ty p pos) = (if ty == Injective then "inj-event " else "event ") ++
                                show e ++ "(" ++ show x ++ ");\n" ++ show p
    show (Lock s p pos) = "lock(" ++ showSep "," s ++ ");\n" ++ show p
    show (Unlock s p pos) = "unlock(" ++ showSep "," s ++ ");\n" ++ show p

data SetExp = SetIn Term Set
            | And SetExp SetExp
            | Or SetExp SetExp
            | Not SetExp
              deriving (Eq)

instance Show SetExp where
    show (SetIn t s) = show t ++ " in " ++ show s
    show (And b1 b2) = "(" ++ show b1 ++ " and " ++ show b2 ++ ")"
    show (Or b1 b2) = "(" ++ show b1 ++ " or " ++ show b2 ++ ")"
    show (Not b) = "not " ++ show b

data FuncDecl = FuncDecl { funcDeclName :: String, funcDeclArgs :: [Term], funcDeclPrivate :: Bool, funcDeclPos :: SourcePos }
                deriving (Eq)

instance Show FuncDecl where
    show (FuncDecl name args private pos) = "fun " ++ name ++ "/" ++ show (List.length args) ++ "."

data PredDecl = PredDecl { predDeclName :: String, predDeclArgs :: Integer }
                deriving (Eq)

instance Show PredDecl where
    show (PredDecl name args) = "pred " ++ name ++ "/" ++ show args ++ "."

data SetDecl = SetDecl { setDeclName :: String, setDeclType :: Term, setDeclSetType :: SetDeclType, setDeclPos :: SourcePos }
               deriving (Eq, Show)

data SetDeclType = SetDeclT | EventDeclT | InjEventDeclT
                   deriving (Eq, Show)

data Reduc = Reduc { reducName :: String, reducArgs :: [Term], reducRes :: Term, reducPrivate :: Bool, reducPos :: SourcePos }
             deriving (Eq)

instance Show Reduc where
    show (Reduc name args res private pos) = "reduc " ++ name ++ "(" ++ showSep "," args ++ ") = " ++ show res

data Set = Set { setName :: String }
           deriving (Eq, Ord)

instance Show Set where
    show (Set n) = n

data Query = Query { queryPred :: Pred, queryState :: Maybe SetExp, queryGamma :: Gamma }
             deriving (Eq, Show)

data System = System { systemProcess :: Process,
                       systemPubNames :: Process,
                       systemReducs :: [Reduc],
                       systemSets :: [SetDecl],
                       systemQueries :: [Query],
                       systemFuncDecls :: [FuncDecl],
                       systemTypes :: [Term],
                       systemParams :: [Param]}
              deriving (Eq, Show)

data Param = PVParam { pvParamName :: String, pvParamValue :: String }
             deriving(Eq)
instance Show Param where
    show (PVParam k v) = "param " ++ k ++ " = " ++ v ++ ".\n"

restrictProcess Nil p = p
restrictProcess (New x t cont pos) p = New x t (restrictProcess cont p) pos

type Subst = Map.Map Term Term
type Gamma = Subst

data PrologProgram = PrologProgram { prologQueries :: [Pred],
                                     prologNoUnif :: [Pred],
                                     prologClauses :: [Clause],
                                     prologFuncDecls :: [FuncDecl],
                                     prologPredDecls :: [PredDecl],
                                     prologParams :: [Param] }
                     deriving (Eq, Show)

indent :: [Char] -> [Char]
indent source = List.unlines newlines
    where lines = List.lines source
          newlines = List.map (\ x -> "  " ++ x) lines

freshVar :: Term -> State Int Term
freshVar (Var x) =
    do i <- get
       let x' = x ++ "__" ++ show i
       put $ i+1
       return (Var x')
freshVar t =
    error "freshVar only works on variables"

stdFreshVar :: Term -> State (Int, Subst) Term
stdFreshVar (Var x) =
    do (i, rho) <- get
       let x' = x ++ "__" ++ show i
       put (i+1, rho)
       return (Var x')

concatMapM        :: (Monad m) => (a -> m [b]) -> [a] -> m [b]
concatMapM f xs   =  liftM concat (mapM f xs)


traceThis :: (Show a) => a -> a
traceThis x = trace (show x) x
traceThisM :: (Show a) => String -> a -> a
traceThisM m x = trace (m ++ show x) x
warning :: String -> SourcePos -> a -> a
warning msg pos = trace (sourceName pos ++ ":" ++ show (sourceLine pos) ++
                         ":" ++ show (sourceColumn pos) ++ ": " ++ msg)
