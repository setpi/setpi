{-# LANGUAGE DoAndIfThenElse #-}
module Main where

import System.Environment
import System.IO
import System.Process
import System.Random
import Debug.Trace

import ParsePi
import SetPi
import Types
import TypeCheck
import ProVerifOut
import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import Control.Monad.State

main = do
  args <- getArgs
  case args of
    [f] -> processPi f
    _   -> usage

processPi :: String -> IO ()
processPi f = do
  parseResult <- parsePi f
  case parseResult of
    Right sys ->
        do -- putStrLn $ show sys
           let errors = typeCheck sys
           if not (List.null errors) then
               error ("Type checking failed\n" ++
                      List.intercalate "\n" (List.map show errors))
           else do
               let program = translate sys
               let sProgram = outProVerif program
               -- putStrLn $ "Generated ProVerif program:\n"
               -- putStrLn $ sProgram
               -- putStrLn $ show $ List.map setDeclName $ systemSets sys

               putStrLn $ "\n\nProVerif output:\n"
               withTempFile "setpi.horn" (callProVerif sProgram)
    Left e ->
        do putStrLn $ show e

withTempFile :: String -> (FilePath -> IO ()) -> IO ()
withTempFile template f = do
  let (base, '.':ext) = List.splitAt (Maybe.fromJust $ List.elemIndex '.' template) template
  i <- getStdRandom random :: IO Int
  let filePath = (base ++ show i ++ '.' : ext)
  f filePath
  rawSystem "rm" [filePath]
  return ()

callProVerif :: String -> FilePath -> IO ()
callProVerif program filePath = do
  writeFile filePath program
  rawSystem "proverif" [filePath]
  return ()

usage :: IO ()
usage = do
  progName <- getProgName
  putStrLn $ "Usage: " ++ progName ++ " input_file.sp"
