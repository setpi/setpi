type key.
type pid.
type sid.
type channel.
type nonce.
type cnt.

set used_a: cnt.
set used_b: cnt.

event yka_press(cnt).
inj-event yka_login(cnt).

event ykb_press(cnt).
inj-event ykb_login(cnt).

new ch: channel.
new ch_server: channel [private].

fun senc(t, key).
reduc sdec(senc(x,k),k) = x.

query x_pid: pid, x_nonce: nonce, x_sid: sid, x_c: cnt, x_tpr: nonce, k: key;
      msg(ch, (x_pid, x_nonce, senc((x_sid, x_c, x_tpr), k)))
      where (x_c in yka_login and not x_c in yka_press) or
      	    (x_c in yka_login_twice).

query x_pid: pid, x_nonce: nonce, x_sid: sid, x_c: cnt, x_tpr: nonce, k: key;
      msg(ch, (x_pid, x_nonce, senc((x_sid, x_c, x_tpr), k)))
      where (x_c in ykb_login and not x_c in ykb_press) or
      	    (x_c in ykb_login_twice).

let ButtonPress_A() =
    new x_c : cnt;
    new x_nonce : nonce;
    new x_tpr : nonce;
    lock (yka_press);
    set x_c in yka_press;
    out(ch, (x_pid, x_nonce, senc((x_sid, x_c, x_tpr), k))).

let Yubikey_A() =
    new ch_yubikey : channel;
    new k : key;
    new x_pid : pid;
    new x_sid : sid;
    out (ch_server, (x_pid, x_sid, k));
    out (ch, x_pid);
    !ButtonPress_A().

let Server_A() = !
    in (ch, (x_pid : pid, x_nonce: nonce, x_enc : senc((sid, cnt, nonce), key)));
    in (ch_server, (=x_pid : pid, x_sid : sid, x_k:key));
    let (=x_sid : sid, x_cnt : cnt, x_tpr : nonce) = sdec(x_enc, x_k) in
    lock (used_a, yka_login, yka_login_twice);
    if not x_cnt in used_a then
    if not x_cnt in yka_login then
       set x_cnt in used_a and x_cnt in yka_login
    else
       set x_cnt in used_a and x_cnt in yka_login_twice.

let ButtonPress_B() =
    new x_c : cnt;
    new x_nonce : nonce;
    new x_tpr : nonce;
    lock (ykb_press);
    set x_c in ykb_press;
    out(ch, (x_pid, x_nonce, senc((x_sid, x_c, x_tpr), k))).

let Yubikey_B() =
    new ch_yubikey : channel;
    new k : key;
    new x_pid : pid;
    new x_sid : sid;
    out (ch_server, (x_pid, x_sid, k));
    out (ch, x_pid);
    !ButtonPress_B().

let Server_B() = !
    in (ch, (x_pid : pid, x_nonce: nonce, x_enc : senc((sid, cnt, nonce), key)));
    in (ch_server, (=x_pid : pid, x_sid : sid, x_k:key));
    let (=x_sid : sid, x_cnt : cnt, x_tpr : nonce) = sdec(x_enc, x_k) in
    lock (used_b, ykb_login, ykb_login_twice);
    if not x_cnt in used_b then
    if not x_cnt in ykb_login then
       set x_cnt in used_b and x_cnt in ykb_login
    else
       set x_cnt in used_b and x_cnt in ykb_login_twice.

process (Yubikey_A() | Server_A()) | (Yubikey_B() | Server_B())
