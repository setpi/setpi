type T.
type channel.

set s1 : T.
set s2 : T.

query t:T; att(f(t)) where (t in s1 and t in s2) or (not t in s1 and t in s2).

fun f(T).
reduc g(f(x)) = x.

new ch : channel.

process lock(s1,s2);
	in(ch, x : f(T));
	let y:T = g(x) in
	if not y in s1 then
         set (y in s1)
	else
	 set (y in s2)