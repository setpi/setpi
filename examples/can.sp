type cnt.
type msg.
type key.
type channel.

set received : cnt.
event send(msg).
inj-event accept(msg).

fun sign(t,key).
reduc checksign(x, sign(x, y), y) = x.

query x : msg; inj-agree accept(x) ==> send(x).

new ch: channel.
new k: key [private].

let A = new c: cnt;
      	new m: msg;
	event send(m);
	out(ch, ((c, m), sign((c,m), k))).

let B = in(ch, (xc: cnt, xm: msg, xs:sign((cnt, msg), key)));
      	let xr : (cnt, msg) = checksign((xc, xm), xs, k) in
	    if not xc in received then
	       set xc in received;
	       inj-event accept(xm).

process (!A()) | (!{received}B())