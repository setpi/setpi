type Channel.
type Seed.

type Key.

type Msg = msg(Seed).
type Cnt = cnt(Seed).

fun msg(Seed).
fun cnt(Seed).

fun hmac(Key, t).
fun accept(Msg, Cnt) [private].
fun reject(Msg, Cnt) [private].

reduc checksign(sk, hmac(sk, m)) = m.

set sent : msg(Seed).
set seen : cnt(Seed).
set used : msg(Seed).
set used_twice : msg(Seed).

query x:Seed; att(msg(x)) where (not msg(x) in sent) and (msg(x) in used).
query x:Seed; att(msg(x)) where (msg(x) in used_twice).

let M(sk) =
    new seed : Seed;
    let msg : Msg = msg(seed) in
    let cnt : Cnt = cnt(seed) in
    set (msg in sent);
    out(ch, (msg, cnt, hmac(sk, (msg,cnt)))).

let P(sk) =
    in(ch, (x_msg : Msg, x_cnt : Cnt, x_sig : hmac(Key, (Msg, Cnt))));
    if not x_cnt in seen then
      let (=x_msg : Msg, =x_cnt : Cnt) = checksign(sk, x_sig) in
        set (x_cnt in seen);
        out(ch, accept(x_msg, x_cnt));
        if x_msg in used then set (x_msg in used_twice)
        else set (x_msg in used)
    else
      out(ch, reject(x_msg, x_cnt)).

process new ch: Channel; new sk : Key;
        (!{sent}M(sk)) | (!{seen, used, used_twice}P(sk))
