(* ASW Voting protocol *)

type channel.
new ch : channel.

type agent.
new a : agent [private].
new i : agent.
new s : agent [private].

type nonce.
type contract_t = contract(agent, agent).
type msg1_t = msg1(agent, agent, contract_t, nonce).
type h_nonce_t = h(nonce).

event cln_hashed(agent, agent, nonce).
event cln_revealed(agent, agent, nonce).
event cln_aborted(agent, agent, nonce).
event cln_valid(agent, agent, nonce).

event srv_aborted(agent, agent, nonce).
event srv_valid(agent, agent, nonce).

fun msg1(agent, agent, contract_t, nonce).
fun msg2(agent, msg1_t, h_nonce_t).
fun contract(agent, agent).
fun h(nonce).

fun sign(agent, text).
reduc checksign(x_k, sign(x_k, x_m)) = x_m.

let exchange_orig(o, r) =
    new n_o : nonce;
    let me_1 : msg1_t = sign(o, msg1(o, r, contract(o, r), h(n_o))) in
    out(ch, me_1);
    in(ch, me2 : msg2_t);

let exchange_resp(o, r) = 
    new n_r : nonce;
    let (=)