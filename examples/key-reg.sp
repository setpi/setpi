
type Seed.
type PKey = pk(Seed).
type SKey = sk(Seed).
type Bitstring.

set ring_a: SKey.
set valid_a: PKey.
set revoked_a: PKey.

fun pk(Seed).
fun sk(Seed).

fun penc(PKey, t).
fun senc(SKey, t).

reduc pdec(pk(s), senc(sk(s), m)) = m.
reduc sdec(sk(s), penc(pk(s), m)) = m.
reduc eq(x, x) = x.

query s:Seed; att(sk(s))
      where sk(s) in ring_a and pk(s) in valid_a.

new new_key : Bitstring.    
new confirm : Bitstring.
new a : Bitstring.
new ch : Bitstring.
new attack : Bitstring [private].
new s_a : Seed [private].

let A() =
    select sk_a : SKey where sk_a in ring_a;
    new s_a' : Seed;
    let sk_a':SKey = sk(s_a') in
    let pk_a':PKey = pk(s_a') in
    update (sk_a' in ring_a);
    out(ch, senc(sk_a, (new_key, a, pk_a')));
    in(ch, x_c : penc(PKey, Bitstring));
    let x_res:Bitstring = sdec(sk_a', x_c) in
    let x__:Bitstring = eq(x_res, confirm) in
    update (not sk_a in ring_a);
    out(ch, sk_a).

let S() =
    select pk_a : PKey where pk_a in valid_a;
    in(ch, x_s : senc(SKey, (Bitstring, Bitstring, PKey)));
    let (= new_key:BitString, = a:Bitstring, pk_a' : PKey) = pdec(pk_a, x_s) in
    if not pk_a' in valid_a and not pk_a' in revoked_a then
    update (pk_a in revoked_a) and (not pk_a in valid_a) and (pk_a' in valid_a);
    out(ch, penc(pk_a', confirm)).

process let sk_a : SKey = sk(s_a) in
	let pk_a : PKey = pk(s_a) in
	lock(ring_a, valid_a);
	update (sk_a in ring_a) and (pk_a in valid_a);
	unlock(ring_a, valid_a);
	(!{ring_a}A()) | (!{valid_a,revoked_a}S())