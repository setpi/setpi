(*
 * CANAuth example with message and counter originating from the same seed.
 * Setting `impliesOptim = True` in the source code for implies allows proving
 * the injective agreement.
 *)

type seed.
type cnt = cnt(seed).
fun cnt(seed).
type msg = msg(seed).
fun msg(seed).

type key.
type channel.

set received : cnt.
event send(msg).
inj-event accept(msg).

fun sign(t,key).
reduc checksign(x, sign(x, y), y) = x.
reduc sameseed(cnt(x), msg(x)) = x.

query x_seed: seed; inj-agree accept(msg(x_seed)) ==> send(msg(x_seed)).

new ch : channel.
new k : key [private].

let A = new s: seed;
        let c: cnt = cnt(s) in
      	let m: msg = msg(s) in
	event send(m);
	out(ch, (c, m, sign((c, m), k))).

let B = in(ch, (xc: cnt, xm: msg, xs:sign((cnt, msg), key)));
      	let xr : msg = checksign((xc, xm), xs, k) in
	(*let x_ : seed = sameseed(xc, xm) in*)
	if not xc in received then
	    set xc in received;
	    inj-event accept(xm).

process (!A()) | (!{received}B())