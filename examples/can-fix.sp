type cnt.
fun msg(cnt).
type msg = msg(cnt).
reduc getCnt(msg(x)) = x.

type key.
type channel.

set received : cnt.
event send(msg).
inj-event accept(msg).

fun sign(t,key).
reduc checksign(x, sign(x, y), y) = x.

query x_cnt:cnt; inj-agree accept(msg(x_cnt)) ==> send(msg(x_cnt)).
query m:msg; msg(ch, (m, sign(m, k))) where m in accept_twice.

new ch : channel.
new k : key [private].

let A = new c: cnt;
      	let m:msg = msg(c) in
	event send(m);
	out(ch, (m, sign(m, k))).

let B = in(ch, (xm: msg, xs:sign(msg, key)));
        let xc : cnt = getCnt(xm) in
      	let xr : msg = checksign(xm, xs, k) in
	if not xc in received then
	    set xc in received;
	    inj-event accept(xm).

process (!A()) | (!{received}B())