type channel.
type token.
type key.
type bs. (* bitstring *)

fun lproj(bs, token) [private].
fun rproj(bs, token) [private].
fun aenc(t, key).
fun pk(key).

set left: token.
set right: token.

new c: channel.
new req_left: bs.
new req_right: bs.

reduc adec(aenc(x,pk(k)),k) = x.

query z:bs, y:token; att((lproj(z, y),rproj(z, y))) where y in left.
query z:bs, y:token; att((lproj(z, y),rproj(z, y))) where y in right.

process new k: key;
	let pk_k:pk(key) = pk(k) in
	out(c, pk_k) |
	(! {left,right}
	   in(c, (x:bs, y:token));
	   if not y in left and not y in right then
	   ( let =req_left:bs  = x in set y in left else
	     let =req_right:bs = x in set y in right)) |
	(! {left,right}
	   in(c, (x:aenc(bs,pk(key)), y:token));
	   let z:bs = adec(x, k) in
	   if y in left  then out(c, lproj(z, y)) else
	   if y in right then out(c, rproj(z, y)))