type seed.
type ts = ts(seed).
fun ts(seed).
type msg = msg(seed).
fun msg(seed).

type key.
type channel.

set recent : ts.
set old : ts.
event send(msg).
inj-event accept(msg).

fun sign(t,key).
reduc checksign(x, sign(x, y), y) = x.

query x_seed: seed; inj-agree accept(msg(x_seed)) ==> send(msg(x_seed)).

new ch : channel.
new chclock : channel.
new k : key [private].

let A = new s: seed;
        let t: ts = ts(s) in
      	let m: msg = msg(s) in
	lock(recent,old);
	set t in recent;
	event send(m);
	out(ch, (t, m, sign((t, m), k)));
	out(chclock, t);
	unlock(recent,old).

let B = in(ch, (xt: ts, xm: msg, xs:sign((ts, msg), key)));
      	let xr : msg = checksign((xt, xm), xs, k) in
	lock(recent,old);
	if not xt in old then
	    inj-event accept(xm);
	    unlock(recent,old).

let Clock = in(chclock, t: ts); set (not t in recent) and (t in old).

process (!A()) | (!B()) | (!{recent,old}Clock())