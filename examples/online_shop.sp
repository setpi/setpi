type channel.
type good.

fun request(good).
fun cancel(good).
fun processing(good) [private].
fun delivered(good) [private].
fun cancelled(good) [private].

reduc reqGood(request(g)) = g.
reduc cancReq(cancel(request(g))) = request(g).

set processing : request(good).
set cancelled  : request(good).
set delivered  : request(good).

new ch:channel.

query g:good; att(cancelled(request(g))) where request(g) in delivered.
query g:good; att(delivered(request(g))) where request(g) in cancelled.
query g:good; att(cancelled(request(g))) where request(g) in cancelled.

let Shop =
    (! {processing, cancelled, delivered}
       new g : good;
       out(ch, g) ) |
    (! {processing, cancelled, delivered}
       in(ch, req:request(good));
       let g : good = reqGood(req) in
       if not req in processing and not req in cancelled and not req in delivered then
       set req in processing;
       out(ch, processing(g)) )  |
    (! {processing, cancelled, delivered}
       in(ch, req:request(good));
       let g : good = reqGood(req) in
       if req in processing and not req in cancelled and not req in delivered then
       set req in delivered and not req in processing;
       out(ch, delivered(g)) ) |
    (! {processing, cancelled, delivered}
       in(ch, canc:cancel(request(good)));
       let req : request(good) = cancReq(canc) in
       if req in processing and not req in delivered then
       set req in cancelled and not req in processing;
       out(ch, cancelled(req)) ).

process Shop()