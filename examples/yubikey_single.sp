type key.
type pid.
type sid.
type channel.
type nonce.
type cnt.

set used: cnt.

event yk_press(cnt).
inj-event yk_login(cnt).

new ch: channel.
new ch_server: channel [private].

fun senc(t, key).
reduc sdec(senc(x,k),k) = x.

query x_pid: pid, x_nonce: nonce, x_sid: sid, x_c: cnt, x_tpr: nonce, k: key;
      att((x_pid, x_nonce, senc((x_sid, x_c, x_tpr), k)))
      where (x_c in yk_login and not x_c in yk_press) or
      	    (x_c in yk_login_twice).

let ButtonPress() =
    new x_c : cnt;
    new x_nonce : nonce;
    new x_tpr : nonce;
    lock (yk_press);
    set x_c in yk_press;
    out(ch, (x_pid, x_nonce, senc((x_sid, x_c, x_tpr), k))).

let Yubikey() =
    new ch_yubikey : channel;
    new k : key;
    new x_pid : pid;
    new x_sid : sid;
    out (ch_server, (x_pid, x_sid, k));
    out (ch, x_pid);
    !ButtonPress().

let Server() = !
    in (ch, (x_pid : pid, x_nonce: nonce, x_enc : senc((sid, cnt, nonce), key)));
    in (ch_server, (=x_pid : pid, x_sid : sid, x_k:key));
    let (=x_sid : sid, x_cnt : cnt, x_tpr : nonce) = sdec(x_enc, x_k) in
    lock (used, yk_login, yk_login_twice);
    if not x_cnt in used then
    if not x_cnt in yk_login then
       set x_cnt in used and x_cnt in yk_login
    else
       set x_cnt in used and x_cnt in yk_login_twice.

process (Yubikey() | Server())
